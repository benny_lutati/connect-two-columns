import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import 'jsplumb'


@Component({
  selector: 'graph',
  template: `
    <div #item1 item1> IT1  </div> <br>
    <div #item2 item2> IT2  </div>
  `,
  styles: [`
    div {
      padding: 20px;
      border: 1px solid black;
    }

    [item1] {
      background-color: red;
      display: inline-block;
    }
    
    [item2] {
      margin-top: 100px;
      background-color: blue;
      display: inline-block;
    }
  `]
})
export class Graph implements OnInit {
  private plumb: jsPlumbInstance;

  @ViewChild("item1")
  private it1: ElementRef;

  @ViewChild("item2")
  private it2: ElementRef;

  constructor(private el: ElementRef) {
  }

  ngOnInit(): void {


    console.log("initializing graph", this.el.nativeElement);

    this.plumb = jsPlumb.getInstance({
      // drag options
      DragOptions: {cursor: "pointer", zIndex: 2000} as any,
      // default to a gradient stroke from blue to green.
      PaintStyle: {
        stroke: "#558822",
        strokeWidth: 10
      } as any,
      Container: this.el.nativeElement
    });

    this.plumb.makeSource(this.it1.nativeElement, {
      connector: 'StateMachine'
    } as any);
    this.plumb.makeTarget(this.it2.nativeElement, {
      anchor: 'Continuous'
    });

  }


}

